﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WaterQualityDesktop
{
    public partial class Form1 : Form
    {
        private List<String> mLabels;

        private List<int> values;

        private OpenFileDialog openFileDialog1;

        private static Color[] VORDIPLOM_COLORS = {
            Color.FromArgb(255, 192, 255, 140), Color.FromArgb(255, 255, 247, 140), Color.FromArgb(255, 255, 208, 140),
            Color.FromArgb(255, 140, 234, 255), Color.FromArgb(255, 255, 140, 157)
        };

        private int counter = 0;

        public Form1()
        {
            InitializeComponent();
            mLabels = new List<String>();
            values = new List<int>();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            counter++;
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (var reader = new StreamReader(myStream))
                        {
                            // Insert code to read the stream 
                            String label = openFileDialog1.SafeFileName;
                            int water;
                            List<String> objectives = new List<String>();
                            if (label.Contains(".csv"))
                            {
                                List<List<String>> list = ReadCSVFile(reader);
                                if (Validate(list))
                                {
                                    objectives = list[list.Count - 1];
                                    list.RemoveRange(0, 1);
                                    list.RemoveRange(list.Count - 1, 1);
                                    water = Calculate(list, objectives, label);
                                }
                                else
                                {
                                    water = 0;
                                    label2.Text = "Document is not valid";
                                    label2.BackColor = Color.Red;
                                    return;
                                }
                            }
                            else
                            {
                                List<List<String>> list = ReadXLSFile(reader);
                                if (Validate(list))
                                {
                                    objectives = list[list.Count - 1];
                                    list.RemoveRange(list.Count - 1, 1);
                                    water = Calculate(list, objectives, label);
                                }
                                else
                                {
                                    water = 0;
                                    label2.Text = "Document is not valid";
                                    label2.BackColor = Color.Red;
                                    return;
                                }
                            }
                            Console.WriteLine("Water Quality: " + water);
                            SetupChart(label, water);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private List<List<String>> ReadXLSFile(StreamReader stream)
        {
            using (var sReader = stream.BaseStream)
            {
                using (var reader = ExcelDataReader.ExcelReaderFactory.CreateOpenXmlReader(sReader))
                {
                    List<String> headers = new List<String>();
                    List<List<String>> list = new List<List<String>>();

                    int index = 0;
                    while (reader.Read())
                    {
                        if(index == 0)
                        {
                            int k = 0;
                            while(k < reader.FieldCount)
                            {
                                String h = reader.GetString(k);
                                headers.Add(h);
                                k++;
                            }
                        }
                        else
                        { 
                            List<String> row = new List<String>();
                            int j = 0;
                            while (j < reader.FieldCount)
                            {
                                if(!reader.IsDBNull(j))
                                {
                                    String objective = (String) reader.GetValue(j).ToString();
                                    row.Add(objective);
                                }
                                else
                                {
                                    row.Add("0");
                                }
                                j++;
                            }
                            list.Add(row);
                        }
                        index++;
                    }
                    return list;
                }
            }
        }

        private List<List<String>> ReadCSVFile(StreamReader reader)
        {
            List<List<String>> list = new List<List<String>>();
            String csvLine;
            while ((csvLine = reader.ReadLine()) != null)
            {
                List<String> row = csvLine.Split(',').ToList();
                list.Add(row);
            }
            return list;
        }

        private bool Validate(List<List<String>> list)
        {
            List<String> header = list[0];
            List<String> objectives = list[list.Count - 1];
            if (header.Count != objectives.Count)
            {
                return false;
            }
            return true;
        }

        private int Calculate(List<List<String>> list, List<String> objectives, String label)
        {
            Console.WriteLine("Calculate:" + list.Count);
            int wrongCounter = 0;
            int totalVariables = 0;
            List<int> variablesFailed = new List<int>();
            List<Double> excursionValues = new List<Double>();
            for (int i = 0; i < list.Count; i++)
            {
                List<String> row = list[i];
                for (int j = 1; j < row.Count; j++)
                {
                    totalVariables++;
                    String objectiveString = objectives[j];
                    double lessThan = 0;
                    double greatThan = 0;
                    if (objectiveString.Contains("<"))
                    {
                        lessThan = Double.Parse(objectiveString.Substring(objectiveString.IndexOf("<") + 1));
                        Console.WriteLine("<: " + lessThan);
                    }
                    else if (objectiveString.Contains(">"))
                    {
                        greatThan = Double.Parse(objectiveString.Substring(objectiveString.IndexOf(">") + 1));
                        Console.WriteLine(">: " + greatThan);
                    }
                    else
                    {
                        String[] values = objectiveString.Split('-');
                        lessThan = Double.Parse(values[1]);
                        greatThan = Double.Parse(values[0]);
                        Console.WriteLine(lessThan + "-" + greatThan);
                    }
                    double var = Double.Parse(row[j]);
                    if (lessThan == 0)
                    {
                        if (var < greatThan)
                        {
                            wrongCounter++;
                            if (!variablesFailed.Contains(j))
                            {
                                variablesFailed.Add(j);
                            }
                            double excursion = CalculateExcursion(var, greatThan);
                            excursionValues.Add(excursion);
                        }
                    }
                    else if (greatThan == 0)
                    {
                        if (var > lessThan)
                        {
                            wrongCounter++;
                            if (!variablesFailed.Contains(j))
                            {
                                variablesFailed.Add(j);
                            }
                            double excursion = CalculateExcursion(var, lessThan);
                            excursionValues.Add(excursion);
                        }
                    }
                    else
                    {
                        if (var < greatThan || var > lessThan)
                        {
                            wrongCounter++;
                            if (!variablesFailed.Contains(j))
                            {
                                variablesFailed.Add(j);
                            }
                            if (var < greatThan)
                            {
                                double excursion = CalculateExcursion(var, greatThan);
                                excursionValues.Add(excursion);
                            }
                            if (var > lessThan)
                            {
                                double excursion = CalculateExcursion(var, lessThan);
                                excursionValues.Add(excursion);
                            }
                        }
                    }
                }
            }
            Console.WriteLine("Failed: " + variablesFailed.Count);
            Console.WriteLine("Objectives: " + objectives.Count);
            Console.WriteLine("Wrong counter: " + wrongCounter);
            Console.WriteLine("Total var: " + totalVariables);
            float f1 = CalculateF1orF2(variablesFailed.Count, objectives.Count - 1);
            Console.WriteLine("F1: " + f1);
            float f2 = CalculateF1orF2(wrongCounter, totalVariables);
            Console.WriteLine("F2: " + f2);
            float nse = CalculateNSE(excursionValues, totalVariables);
            Console.WriteLine("NSE: " + nse);
            float f3 = CalculateF3(nse);
            Console.WriteLine("F3: " + f3);

            int waterQuality = WaterQuality(f1, f2, f3);
            String res = label2.Text + label+ "\nF1=" + f1 + "\nF2=" + f2 + "\nF3=" + f3 + ":\nWQI=" + waterQuality+ "\n";
            label2.Text = res + "\n";
            values.Add(waterQuality);
            return waterQuality;
        }

        private float CalculateF1orF2(int failedVariables, int totalVariables)
        {
            float res = (float)failedVariables / totalVariables;
            return res * 100;
        }

        private double CalculateExcursion(double value, double objective)
        {
            if (value == 0 || objective == 0)
            {
                return -1;
            }
            if (value > objective)
            {
                return (value / objective) - 1;
            }
            else
            {
                return (objective / value) - 1;
            }
        }

        private float CalculateNSE(List<Double> list, int totalValues)
        {
            float s = 0;
            foreach (float d in list)
            {
                s = s + d;
            }
            return s / totalValues;
        }

        private float CalculateF3(float nse)
        {
            return (float)(nse / ((0.01 * nse) + 0.01));
        }

        private int WaterQuality(float f1, float f2, float f3)
        {
            double d = Math.Sqrt(f1 * f1 + f2 * f2 + f3 * f3);
            return (int)(100 - (d / 1.732));
        }

        private int seriesCounter = 0;

        private void SetupChart(String chartName, int value)
        {
            seriesCounter++;
            System.Windows.Forms.DataVisualization.Charting.Series series = new System.Windows.Forms.DataVisualization.Charting.Series();
            series.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
            series.Color = VORDIPLOM_COLORS[counter % VORDIPLOM_COLORS.Length];
            series.YValueMembers = "0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100";
            series.Name = chartName;
            series.Points.AddXY(seriesCounter, value);
            //series.Label = chartName;
            series.IsXValueIndexed = false;

            chart1.Series.Add(series);
            chart1.Series[chartName].AxisLabel = chartName;
            chart1.Invalidate();
        }

        private void chart1_Click(object sender, EventArgs e)
        {
        }
    }
}